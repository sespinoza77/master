﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Entities
{
    public class PaymentTypesCarServicesFilters
    {
        public string Clav_TipoPagoFiltroServicioAuto { get; set; }
        public string Clav_TipoPagoFiltroServicio { get; set; }
        public string Clav_Destino { get; set; }
        public string Clav_Rentadora { get; set; }
        public string Clav_SIPP { get; set; }
        public string DiasRentados { get; set; }
    }
}
