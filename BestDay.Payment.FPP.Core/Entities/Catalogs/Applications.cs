﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Entities.Catalogs
{
    public class Applications
    {
        public string Clav_Aplicacion { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Activo { get; set; }
        public string Vivo { get; set; }
        public string EsMovil { get; set; }
    }
}
