﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Entities.Catalogs
{
    public class CreditCardTypes
    {
        public string Clav_TipoTarjetaCredito { get; set; }
        public string Abreviatura { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Usuario_Alta { get; set; }
        public string Fecha_Alta { get; set; }
        public string Usuario_Cambio { get; set; }
        public string Fecha_Cambio { get; set; }
    }
}
