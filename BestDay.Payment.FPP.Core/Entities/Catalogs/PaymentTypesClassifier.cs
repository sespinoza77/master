﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Entities.Catalogs
{
    public class PaymentTypesClassifier
    {
        public string Clav_Clasificador { get; set; }
        public string Nombre_Clasificador { get; set; }
        public string Orden { get; set; }
    }
}
