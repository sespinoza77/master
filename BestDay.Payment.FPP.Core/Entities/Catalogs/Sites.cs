﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Entities.Catalogs
{
    public class Sites
    {
        public string Clav_Sitio { get; set; }
        public string Nombre_Sitio { get; set; }
        public string Email { get; set; }
        public string Clav_Canal { get; set; }
        public string Clav_Idioma { get; set; }
        public string Dominio { get; set; }
        public string Clav_Compania { get; set; }
        public string Usuario_Alta { get; set; }
        public string Fecha_Alta { get; set; }
        public string Usuario_Cambio { get; set; }
        public string Fecha_Cambio { get; set; }
        public string TieneCampana { get; set; }
    }
}
