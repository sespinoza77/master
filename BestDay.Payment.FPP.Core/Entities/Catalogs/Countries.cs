﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Entities.Catalogs
{
    public class Countries
    {
        public string Clav_Pais { get; set; }
        public string Nombre_Pais { get; set; }
        public string Clav_Continente { get; set; }
        public string Clav_SubDivision { get; set; }
        public string Codigo_ISO { get; set; }
        public string Usuario_Alta { get; set; }
        public string Fecha_Alta { get; set; }
        public string Usuario_Cambio { get; set; }
        public string Fecha_Cambio { get; set; }
        public string Clav_Moneda { get; set; }
    }
}
