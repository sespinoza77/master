﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Entities.Catalogs
{
    public class PaymentTypes
    {
        public string Clav_FP { get; set; }
        public string Clav_FP_Agrupador { get; set; }
        public string Nombre_FP { get; set; }
        public string Pago_Reembolso { get; set; }
        public string Clav_Moneda { get; set; }
        public string Clav_Tipo { get; set; }
        public string EsEfectivo { get; set; }
        public string Porc_Comision_Banco { get; set; }
        public string Clav_Banco { get; set; }
        public string Clav_Clasificador { get; set; }
        public string Clav_Procesador { get; set; }
        public string Activo { get; set; }
        public string EsAmex { get; set; }
        public string EsVisa { get; set; }
        public string EsMC { get; set; }
        public string Clav_FP_Reembolso { get; set; }
        public string Clav_ProcesadorPago { get; set; }
        public string MontoMinimoParaMeses { get; set; }
        public string Usuario_Alta { get; set; }
        public string Fecha_Alta { get; set; }
        public string Usuario_Cambio { get; set; }
        public string Fecha_Cambio { get; set; }
    }
}
