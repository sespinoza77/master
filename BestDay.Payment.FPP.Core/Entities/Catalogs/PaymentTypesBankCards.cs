﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Entities.Catalogs
{
    public class PaymentTypesBankCards
    {
        public string Clav_TipoPagoBancoTarjeta { get; set; }
        public string Clav_FP { get; set; }
        public string Clav_Banco { get; set; }
        public string Clav_TipoTarjetaCredito { get; set; }
        public string Clav_TipoPagoImagen { get; set; }
        public string Clav_ProcesadorPago { get; set; }
        public string Vigencia_Inicial { get; set; }
        public string Vigencia_Final { get; set; }
        public string Activo { get; set; }
        public string Ambiente { get; set; }
        public string Clav_TipoMercante { get; set; }
        public string EsOnline { get; set; }
        public string EsPrivado { get; set; }
    }
}
