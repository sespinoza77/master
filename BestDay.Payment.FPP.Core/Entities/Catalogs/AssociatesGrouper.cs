﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Entities.Catalogs
{
    public class AssociatesGrouper
    {
        public string Clav_Asociado_Agrupador{ get; set; }
        public string Nombre_Asociado_Agrupador { get; set; }
        public string Contrasena { get; set; }
        public string RowGuid { get; set; }
        public string UsuarioAlta { get; set; }
        public string FechaAlta { get; set; }
        public string UsuarioCambio { get; set; }
        public string FechaCambio { get; set; }
    }
}
