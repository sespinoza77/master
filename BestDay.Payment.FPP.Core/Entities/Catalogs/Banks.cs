﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Entities.Catalogs
{
    public class Banks
    {
        public string Clav_Banco { get; set; }
        public string Abreviatura { get; set; }
        public string Nombre { get; set; }
        public string Id_SAP { get; set; }
    }
}
