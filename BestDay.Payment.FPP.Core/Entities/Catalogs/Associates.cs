﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Entities.Catalogs
{
    public class Associates
    {
        public string Clav_Asociado { get; set; }
        public string Nombre_Asociado { get; set; }
        public string Fecha_Ingreso { get; set; }
        public string Clav_Canal { get; set; }
        public string Clav_Ciudad { get; set; }
        public string Clav_Estado { get; set; }
        public string Clav_Pais { get; set; }
        public string Codigo_Postal { get; set; }
        public string Telefono { get; set; }
        public string Contacto { get; set; }
        public string Email { get; set; }
        public string URL { get; set; }
        public string Clav_Mercado { get; set; }
        public string Usuario_Alta { get; set; }
        public string Fecha_Alta { get; set; }
        public string Usuario_Cambio { get; set; }
        public string Fecha_Cambio { get; set; }
        public string EsMovil { get; set; }
    }
}
