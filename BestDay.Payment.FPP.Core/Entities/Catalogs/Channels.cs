﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Entities.Catalogs
{
    public class Channels
    {
        public string Clav_Canal { get; set; }
        public string Clav_Canal_Agrupador { get; set; }
        public string Nombre_Canal { get; set; }
        public string Clav_Asociado { get; set; }
        public string Activo { get; set; }
        public string Usuario_Alta { get; set; }
        public string Fecha_Alta { get; set; }
        public string Usuario_Cambio { get; set; }
        public string Fecha_Cambio { get; set; }
        public string Clav_Compania { get; set; }
    }
}
