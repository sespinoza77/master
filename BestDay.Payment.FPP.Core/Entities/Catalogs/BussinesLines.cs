﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Entities.Catalogs
{
    public class BussinesLines
    {
        public string ID { get; set; }
        public string Nombre { get; set; }
        public string CodigoGPI { get; set; }
        public string TipoCliente { get; set; }
        public string Id_ClienteGenerico { get; set; }
        public string Activo { get; set; }
    }
}
