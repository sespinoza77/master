﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Entities.Catalogs
{
    public class PaymentTypesMethods
    {
        public string Clav_TipoMedioPago { get; set; }
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public string Activo { get; set; }
    }
}
