﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Entities.Catalogs
{
    public class Companies
    {
        public string Clav_Compania { get; set; }
        public string Nombre_Compania { get; set; }
        public string Clav_Moneda_Funcional { get; set; }
        public string Clav_Pais { get; set; }
        public string Clav_InterfaceContable { get; set; }
        public string Usuario_Alta { get; set; }
        public string Fecha_Alta { get; set; }
        public string Usuario_Cambio { get; set; }
        public string Fecha_Cambio { get; set; }
        public string Clav_Estado { get; set; }
        public string Razon_Social { get; set; }
        public string Email { get; set; }
        public string Id_SAP { get; set; }
    }
}
