﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Entities
{
    public class PaymentTypesTransferServicesFilters
    {
        public string Clav_TipoPagoFiltroServicio { get; set; }
        public string Clav_GrupoTipoServicio { get; set; }
        public string Clav_Destino { get; set; }
        public string Clav_Ciudad { get; set; }
        public string Clav_Traslado { get; set; }
        public string TipoTraslado { get; set; }
    }
}
