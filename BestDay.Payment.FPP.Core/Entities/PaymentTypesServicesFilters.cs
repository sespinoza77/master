﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Entities
{
    public class PaymentTypesServicesFilters
    {
        public string Clav_TipoPagoFiltroServicio { get; set; }
        public string Clav_GrupoTipoServicio { get; set; }
        public string FechaInicioServicio { get; set; }
        public string FechaFinServicio { get; set; }
        public string Clav_Pais { get; set; }
        public string Adultos { get; set; }
        public string Infantes { get; set; }
        public string Margen { get; set; }
        public string Activo { get; set; }
        public string Descripcion { get; set; }
        public string UsuarioAlta { get; set; }
        public string FechaAlta { get; set; }
        public string UsuarioCambio { get; set; }
        public string FechaCambio { get; set; }
    }
}
