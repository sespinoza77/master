﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Entities.Responses.PaymentTypesBussinesFiltersResponses {
    public class GetPaymentTypesBussinesFiltersByIdResponse : Responses   
    {
        /// <summary>
        /// The specific Payment Types Bussines Filter
        /// </summary>
        public PaymentTypesBussinesFilters PaymentTypesBussinesFilter { get; set; }
    }
}
