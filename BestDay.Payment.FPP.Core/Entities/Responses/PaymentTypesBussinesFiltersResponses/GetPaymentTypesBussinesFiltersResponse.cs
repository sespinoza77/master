﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Entities.Responses.PaymentTypesBussinesFiltersResponses
{
    public class GetPaymentTypesBussinesFiltersResponse : Responses
    {
        /// <summary>
        /// All the Payment Types Bussines Filters
        /// </summary>
        public List<PaymentTypesBussinesFilters> PaymentTypesBussinesFiltersList { get; set; }
    }
}
