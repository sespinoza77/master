﻿using System;
using System.Collections.Generic;
using System.Text;
using BestDay.Payment.FPP.Core.Entities;

namespace BestDay.Payment.FPP.Core.Entities.Responses.PaymentTypesFiltersResponses
{
    public class GetPaymentTypesFiltersByIdResponse : Responses
    {
        /// <summary>
        /// The specific Payment Types Filter
        /// </summary>
        public PaymentTypesFilters PaymentTypesBussinesFilter { get; set; }
    }
}
