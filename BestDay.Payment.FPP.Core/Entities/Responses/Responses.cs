﻿using BestDay.Payment.FPP.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Entities.Responses
{
    public class Responses
    {
        /// <summary>
        /// The status of the response
        /// </summary>
        public StatusEnum Status { get; set; }

        /// <summary>
        /// The description of the response if it fails or if it was correct
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// A short message of the response
        /// </summary>
        public string Message { get; set; }

    }
}
