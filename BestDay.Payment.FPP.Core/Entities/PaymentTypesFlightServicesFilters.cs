﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Entities
{
    public class PaymentTypesFlightServicesFilters
    {
        public string Clav_TipoPagoFiltroServicioAereo { get; set; }
        public string Clav_TipoPagoFiltroServicio { get; set; }
        public string Clav_Aerolinea { get; set; }
        public string Clav_AeropuertoOrigen { get; set; }
        public string Clav_AeropuertoDestino { get; set; }
        public string TipoVuelo { get; set; }
        public string TipoTarifa { get; set; }
    }
}
