﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Entities
{
    public class PaymentTypesHotelServicesFilters
    {
        public string Clav_TipoPagoFiltroServicioHotel { get; set; }
        public string Clav_TipoPagoFiltroServicio { get; set; }
        public string Clav_Destino { get; set; }
        public string Clav_Cadena { get; set; }
        public string Clav_Hotel { get; set; }
        public string Clav_Cuarto { get; set; }
        public string Clav_Plan { get; set; }
        public string Clav_Mercado { get; set; }
        public string Id_Contrato { get; set; }
        public string Noches { get; set; }
    }
}
