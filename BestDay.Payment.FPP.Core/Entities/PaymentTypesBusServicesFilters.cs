﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Entities
{
    public class PaymentTypesBusServicesFilters
    {
        public string Clav_TipoPagoFiltroServicioAutobus { get; set; }
        public string Clav_TipoPagoFiltroServicio { get; set; }
        public string Clav_AutobusLinea { get; set; }
        public string Clav_TerminalOrigen { get; set; }
        public string Clav_TerminalDestino { get; set; }
        public string TipoViaje { get; set; }
    }
}
