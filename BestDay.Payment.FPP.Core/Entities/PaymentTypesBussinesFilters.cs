﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Entities
{
    public class PaymentTypesBussinesFilters
    {
        public string Clav_TipoPagoFiltroNegocio { get; set; }
        public string Clav_Pais { get; set; }
        public string Clav_Compania { get; set; }
        public string Clav_Aplicacion { get; set; }
        public string Clav_LineaNegocio { get; set; }
        public string Clav_AsociadoAgrupador { get; set; }
        public string Clav_Asociado { get; set; }
        public string Clav_Sitio { get; set; }
        public string Clav_Canal { get; set; }
        public string Incluyente { get; set; }
        public string Activo { get; set; }
        public string UsuarioAlta { get; set; }
        public string FechaAlta { get; set; }
        public string UsuarioCambio { get; set; }
        public string FechaCambio { get; set; }
    }
}
