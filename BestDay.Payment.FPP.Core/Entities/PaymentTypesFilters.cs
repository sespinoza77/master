﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Entities
{
    public class PaymentTypesFilters
    {
        public string Clav_TipoPagoFiltro { get; set; }
        public string Nombre { get; set; }
        public string Clav_TipoMedioPago { get; set; }
        public string Clav_TipoPagoClasificador { get; set; }
        public string Clav_TipoPago { get; set; }
        public string Clav_TipoTarjetaCredito { get; set; }
        public string Clav_Banco { get; set; }
        public string Clav_TipoPagoBancoTarjeta { get; set; }
        public bool Activo { get; set; }
    }
}
