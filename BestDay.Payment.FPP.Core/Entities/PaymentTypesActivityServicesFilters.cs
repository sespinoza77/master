﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Entities
{
    public class PaymentTypesActivityServicesFilters
    {
        public string Clav_TipoPagoFiltroServicioActividad { get; set; }
        public string Clav_TipoPagoFiltroServicio { get; set; }
        public string Clav_Destino { get; set; }
        public string Clav_Tour { get; set; }
        public string Clav_Servicio { get; set; }
    }
}
