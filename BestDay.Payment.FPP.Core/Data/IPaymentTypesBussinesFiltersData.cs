﻿using BestDay.Payment.FPP.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Data
{
    public interface IPaymentTypesBussinesFiltersData
    {
        /// <summary>
        /// This method go to the database and gets all the Payment Types Bussines Filters
        /// </summary>
        /// <returns></returns>
        List<PaymentTypesBussinesFilters> GetAllPaymentTypesBussinesFilters();
        /// <summary>
        /// This method go to the database and gets the Payment Types Bussines Filter whith the specific Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        PaymentTypesBussinesFilters GetPaymentTypesBussinesFilterById(string Id);
        /// <summary>
        /// This method go to the database and gets the Payment Types Bussines Filter whith the specific Id
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        List<PaymentTypesBussinesFilters> FilterPaymentTypesBussinesFilters(string text);
        /// <summary>
        /// This method go to the database and update the specific Payment Types Bussines Filter
        /// </summary>
        /// <param name="paymentTypesBussinesFilter"></param>
        /// <returns></returns>
        bool UpdatePaymentTypesBussinesFilter(PaymentTypesBussinesFilters paymentTypesBussinesFilter);
        /// <summary>
        /// This method go to the database and insert the specific Payment Types Bussines Filter
        /// </summary>
        /// <param name="paymentTypesBussinesFilter"></param>
        /// <returns></returns>
        bool UpdatePaymentTypesBussinesFilters(PaymentTypesBussinesFilters paymentTypesBussinesFilter);
        /// <summary>
        /// This method go to the database and gets the Payment Types Bussines Filter whith the specific Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        bool DeletePaymentTypesBussinesFilter(string Id);
    }
}
