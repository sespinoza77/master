﻿using BestDay.Payment.FPP.Core.Entities.Catalogs;
using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Data
{
    /// <summary>
    /// This methods gets, edit and insert all the data of PaymentTypes
    /// </summary>
    public interface IPaymentTypesData
    {
        /// <summary>
        /// This method go to the database and gets all the Payment Types
        /// </summary>
        /// <returns></returns>
        List<PaymentTypes> GetAllPaymentTypes();
        /// <summary>
        /// This method go to the database and gets the Payment Type whith the specific Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        List<PaymentTypes> GetPaymentTypesById(string Id);
        /// <summary>
        /// This method go to the database and gets the Payment Type whith the specific Id
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        List<PaymentTypes> FilterPaymentTypes(string text);
        /// <summary>
        /// This method go to the database and update the specific Payment Type
        /// </summary>
        /// <param name="paymentType"></param>
        /// <returns></returns>
        bool UpdatePaymentTypes(PaymentTypes paymentType);
        /// <summary>
        /// This method go to the database and insert the specific Payment Type
        /// </summary>
        /// <param name="paymentType"></param>
        /// <returns></returns>
        bool InsertPaymentTypes(PaymentTypes paymentType);
        /// <summary>
        /// This method go to the database and gets the Payment Type whith the specific Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        bool DeletePaymentType(string Id);
    }
}
