﻿using BestDay.Payment.FPP.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Data
{
    public interface IPaymentTypesServicesFiltersData
    {
        /// <summary>
        /// This method go to the database and gets all the Payment Types Services Filters
        /// </summary>
        /// <returns></returns>
        List<PaymentTypesServicesFilters> GetAllPaymentTypesServicesFilters();
        /// <summary>
        /// This method go to the database and gets the Payment Types Services Filter whith the specific Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        List<PaymentTypesServicesFilters> GetPaymentTypesServicesFilterById(string Id);
        /// <summary>
        /// This method go to the database and gets the Payment Types Services Filter whith the specific Id
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        List<PaymentTypesServicesFilters> FilterPaymentTypesServicesFilters(string text);
        /// <summary>
        /// This method go to the database and update the specific Payment Types Services Filter
        /// </summary>
        /// <param name="paymentTypesServicesFilter"></param>
        /// <returns></returns>
        bool UpdatePaymentTypesServicesFilter(PaymentTypesServicesFilters paymentTypesServicesFilter);
        /// <summary>
        /// This method go to the database and insert the specific Payment Types Services Filter
        /// </summary>
        /// <param name="paymentTypesServicesFilter"></param>
        /// <returns></returns>
        bool InsertPaymentTypesServicesFilters(PaymentTypesServicesFilters paymentTypesServicesFilter);
        /// <summary>
        /// This method go to the database and gets the Payment Types Services Filter whith the specific Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        bool DeletePaymentTypesServicesFilter(string Id);
    }
}
