﻿using BestDay.Payment.FPP.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Data
{
    public interface IPaymentTypesFiltersData
    {
        /// <summary>
        /// This method go to the database and gets all the Payment Types Filters
        /// </summary>
        /// <returns></returns>
        List<PaymentTypesFilters> GetAllPaymentTypesFilters();
        /// <summary>
        /// This method go to the database and gets the Payment Types Filter whith the specific Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        List<PaymentTypesFilters> GetPaymentTypesFilterById(string Id);
        /// <summary>
        /// This method go to the database and gets the Payment Types Filter whith the specific Id
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        List<PaymentTypesFilters> FilterPaymentTypesFilters(string text);
        /// <summary>
        /// This method go to the database and update the specific Payment Types Filter
        /// </summary>
        /// <param name="paymentTypesFilter"></param>
        /// <returns></returns>
        bool UpdatePaymentTypesFilter(PaymentTypesFilters paymentTypesFilter);
        /// <summary>
        /// This method go to the database and insert the specific Payment Types Filter
        /// </summary>
        /// <param name="paymentTypeFilter"></param>
        /// <returns></returns>
        bool InsertPaymentTypesFilters(PaymentTypesFilters paymentTypesFilter);
        /// <summary>
        /// This method go to the database and gets the Payment Types Filter whith the specific Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        bool DeletePaymentTypesFilter(string Id);
    }
}
