﻿using BestDay.Payment.FPP.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Data
{
    public interface IPaymentTypesFlightServicesFiltersData
    {
        /// <summary>
        /// This method go to the database and gets all the Payment Types Flight Services Filters
        /// </summary>
        /// <returns></returns>
        List<PaymentTypesFlightServicesFilters> GetAllPaymentTypesFlightServicesFilters();
        /// <summary>
        /// This method go to the database and gets the Payment Types Flight Services Filter whith the specific Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        List<PaymentTypesFlightServicesFilters> GetPaymentTypesFlightServicesFilterById(string Id);
        /// <summary>
        /// This method go to the database and gets the Payment Types Flight Services Filter whith the specific Id
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        List<PaymentTypesFlightServicesFilters> FilterPaymentTypesFlightServicesFilters(string text);
        /// <summary>
        /// This method go to the database and update the specific Payment Types Flight Services Filter
        /// </summary>
        /// <param name="paymentTypesFlightServicesFilter"></param>
        /// <returns></returns>
        bool UpdatePaymentTypesFlightServicesFilter(PaymentTypesFlightServicesFilters paymentTypesFlightServicesFilter);
        /// <summary> 
        /// This method go to the database and insert the specific Payment Types Flight Services Filter
        /// </summary>
        /// <param name="paymentTypesFlightServicesFilter"></param>
        /// <returns></returns>
        bool InsertPaymentTypesFlightServicesFilters(PaymentTypesFlightServicesFilters paymentTypesFlightServicesFilter);
        /// <summary>
        /// This method go to the database and gets the Payment Types Flight Services Filter whith the specific Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        bool DeletePaymentTypesFlightServicesFilter(string Id);
    }
}
