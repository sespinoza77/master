﻿using BestDay.Payment.FPP.Core.Data;
using BestDay.Payment.FPP.Core.Entities;
using BestDay.Payment.FPP.Core.Entities.Enums;
using BestDay.Payment.FPP.Core.Entities.Responses.PaymentTypesBussinesFiltersResponses;
using System;
using System.Collections.Generic;
using System.Text;

namespace BestDay.Payment.FPP.Core.Manager
{
    public class PaymentTypesBussinesFiltersManager
    {
        #region Members

        /// <summary>
        /// Read only member to use all the PaymentTypesBussinesFilters data functionallity
        /// </summary>
        private readonly IPaymentTypesBussinesFiltersData _paymentTypesBussinesFiltersData;

        /// <summary>
        /// Read only member for use the logging functionality
        /// </summary>
        //private readonly ILoggingManager _logging;

        #endregion Members

        #region Constructor

        /// <summary>
        /// This Constructor receive the business and the logging instances assigned to members
        /// </summary>
        /// <param name="paymentTypesBussinesFiltersData"></param>
        /// <param name="logging"></param>
        public PaymentTypesBussinesFiltersManager(IPaymentTypesBussinesFiltersData paymentTypesBussinesFiltersData/*, ILoggingManager logging*/) {
            _paymentTypesBussinesFiltersData = paymentTypesBussinesFiltersData;
            //_logging = logging;
        }

        #endregion Constructor

        #region Implements

        /// <summary>
        /// Adds information to the union tables and insert it on Payment Types Bussines Filters
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public PaymentTypesBussinesFiltersResponse Add(PaymentTypesBussinesFilters request) {
            if (request != null) {
                var isSuccess = _paymentTypesBussinesFiltersData.UpdatePaymentTypesBussinesFilters(request);
                if (isSuccess)
                    return new PaymentTypesBussinesFiltersResponse { Description = StatusEnum.Success.ToString(), Message = "Se ha agregado correctamente la información a la tabla", Status = StatusEnum.Success };
                //    _logging.WriteLogEntry(TraceEventType.Error, "No se ingreso el registro a la tabla. request = {0}", SerializeObjectToXmlString(request));
                return new PaymentTypesBussinesFiltersResponse { Description = StatusEnum.Pending.ToString(), Message = "No se ingreso el registro a la tabla", Status = StatusEnum.Pending };
            }
            //_logging.WriteLogEntry(TraceEventType.Error, "Los datos de entrada son incorrectos. request = {0}", SerializeObjectToXmlString(request));
            return new PaymentTypesBussinesFiltersResponse { Description = StatusEnum.Fail.ToString(), Message = "Los datos de entrada son incorrectos", Status = StatusEnum.Fail };
        }

        /// <summary>
        /// Method to get All the Payment Types Bussines Filters information
        /// </summary>
        /// <returns></returns>
        public GetPaymentTypesBussinesFiltersResponse GetAll() {
                var listPaymentTypesBussinesFilters = _paymentTypesBussinesFiltersData.GetAllPaymentTypesBussinesFilters();
                if (listPaymentTypesBussinesFilters.Count > 0)
                    return new GetPaymentTypesBussinesFiltersResponse {
                        PaymentTypesBussinesFiltersList = listPaymentTypesBussinesFilters,
                        Description = StatusEnum.Success.ToString(),
                        Message =
                        $"Se encontrarón {listPaymentTypesBussinesFilters.Count} registros en tipos pagos filtros negocios",
                        Status = StatusEnum.Success
                    };
                return new GetPaymentTypesBussinesFiltersResponse { Description = StatusEnum.Pending.ToString(), Message = "No se encuentra información de tipos pagos filtros negocios", Status = StatusEnum.Pending };
        }

        /// <summary>
        /// Method to get the specific Payment Types Bussines Filter information
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public GetPaymentTypesBussinesFiltersByIdResponse GetById(string id) {
            var paymentTypesBussinesFilter = _paymentTypesBussinesFiltersData.GetPaymentTypesBussinesFilterById(id);
            if (paymentTypesBussinesFilter != null)
                return new GetPaymentTypesBussinesFiltersByIdResponse {
                    PaymentTypesBussinesFilter = paymentTypesBussinesFilter,
                    Description = StatusEnum.Success.ToString(),
                    Message =
                    $"Se encontrarón el registro {paymentTypesBussinesFilter.Clav_TipoPagoFiltroNegocio} en tipos pagos filtros negocios",
                    Status = StatusEnum.Success
                };
            return new GetPaymentTypesBussinesFiltersByIdResponse { Description = StatusEnum.Pending.ToString(), Message = "No se encuentra información de tipos pagos filtros negocios", Status = StatusEnum.Pending };
        }

        /// <summary>
        /// Edits information in the union tables and do an update on Payment Types Bussines Filters
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public PaymentTypesBussinesFiltersResponse Edit(PaymentTypesBussinesFilters request) {
            if (request != null) {
                var isSuccess = _paymentTypesBussinesFiltersData.UpdatePaymentTypesBussinesFilters(request);
                if (isSuccess)
                    return new PaymentTypesBussinesFiltersResponse { Description = StatusEnum.Success.ToString(), Message = "Se ha editado correctamente la información en la tabla", Status = StatusEnum.Success };
                //    _logging.WriteLogEntry(TraceEventType.Error, "No se ingreso el registro a la tabla. request = {0}", SerializeObjectToXmlString(request));
                return new PaymentTypesBussinesFiltersResponse { Description = StatusEnum.Pending.ToString(), Message = "No se editado el registro en la tabla", Status = StatusEnum.Pending };
            }
            //_logging.WriteLogEntry(TraceEventType.Error, "Los datos de entrada son incorrectos. request = {0}", SerializeObjectToXmlString(request));
            return new PaymentTypesBussinesFiltersResponse { Description = StatusEnum.Fail.ToString(), Message = "Los datos de entrada son incorrectos", Status = StatusEnum.Fail };
        }

        #endregion Implements
    }
}
