﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BestDay.Payment.FPP.Interface.Controllers
{
    [Route("[controller]")]
    public class PaymentTypesController : ControllerBase
    {
        [HttpGet("/Payments")]
        public JsonResult GetPayments() {
            return new JsonResult(new List<object>() { new { Name= "value1" }, new { Name= "value2" } });
        }
        [HttpPost("/Postperro")]
        public JsonResult TestPost([FromForm] string request) {
            return new JsonResult(new List<object>() { new { Name= "FromBody" }, new { Name= "Perro" } });
        }
        [HttpGet("/Perro")]
        public IEnumerable<string> Test() {
            return new string[] { "Hola", "Perro" };
        }
    }
}