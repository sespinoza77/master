﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BestDay.Payment.FPP.Interface.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class PaymentsTypesController : ControllerBase
    {
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get() {
            return new string[] { "value1", "value2" };
        }
    }
}